﻿#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    starter();
}

void MainWindow::starter()
{
    createNewBMPcolor16m("no");
    openAndPrintBMPcolor16m();
    createNewBMPcolor16m("f3x3","-3x3");
    openAndPrintBMPcolor16m_3x3();
    createNewBMPcolor16m("f5x5","-5x5");
    openAndPrintBMPcolor16m_5x5();
    createNewBMPcolor16m("difference","-difference");
    openAndPrintBMPcolor16m_difference();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::readRawMatrix(string location)
{
    ifstream file;
    file.open(location,ios::binary|ios::in);

    file.seekg(0, std::ios::end);
    lengthOfReadFile = file.tellg();
    file.seekg(0, std::ios::beg);

    BitmapFull = new BYTE[lengthOfReadFile];
    if (file.is_open()) {
        file.read((char*)BitmapFull,lengthOfReadFile);
        file.close();
    }
}

BITMAPFILEHEADER MainWindow::createFileHeader(unsigned int offset)
{
    BITMAPFILEHEADER header;

    header.bfType = 0x4d42;
    header.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPV5HEADER) + 0x4*offset + lengthOfReadFile;
    header.bfReserved1 = 0;
    header.bfReserved2 = 0;
    header.bfOffbits = 0x8A + 0x4*offset;

    return header;
}

BITMAPV5HEADER MainWindow::createPV5Header(unsigned int BitCount)
{
    BITMAPV5HEADER header;

    header.bV5Size = 0x7C;
    header.bV5Width = 640;
    header.bV5Height = 480;
    header.bV5Planes = 1;
    if (BitCount == 1){
        header.bV5BitCount = 1;
    }
    if (BitCount == 4){
        header.bV5BitCount = 4;
    }
    if (BitCount == 8){
        header.bV5BitCount = 8;
    }
    if (BitCount == 24){
        header.bV5BitCount = 24;
    }
    header.bV5Compression = 0;
    header.bV5SizeImage = lengthOfReadFile;
    header.bV5XPelsPerMeter = 160;
    header.bV5YPelsPerMeter = 160;
    header.bV5ClrUsed = 0;
    header.bV5ClrImportant = 0;
    header.bV5RedMask = 0x00FF0000;
    header.bV5GreenMask = 0x0000FF00;
    header.bV5BlueMask = 0x000000FF;
    header.bV5AlphaNask = 0xFF000000;
    header.bV5CSType = 0x73524742;     //'sRGB'

    //if 'sRGB' bV5Endpoints doesn't need
    //header.bV5Endpoints;
    header.bV5Endpoints.ciexyzRed.ciexyzX = 0.64;
    header.bV5Endpoints.ciexyzRed.ciexyzY = 0.33;
    header.bV5Endpoints.ciexyzRed.ciexyzZ = 0.03;
    header.bV5Endpoints.ciexyzGreen.ciexyzX = 0.30;
    header.bV5Endpoints.ciexyzGreen.ciexyzY = 0.60;
    header.bV5Endpoints.ciexyzGreen.ciexyzZ = 0.10;
    header.bV5Endpoints.ciexyzBlue.ciexyzX = 0.15;
    header.bV5Endpoints.ciexyzBlue.ciexyzY = 0.06;
    header.bV5Endpoints.ciexyzBlue.ciexyzZ = 0.79;


    header.bV5GammaRed = 0;
    header.bV5GammaGreen = 0;
    header.bV5GammaBlue = 0;

    header.bV5Intent = 4;
    header.bV5ProfileData = 0;
    header.bV5ProfileSize = 0;
    header.bV5Reserved = 0;

    return header;
}

void MainWindow::createNewBMPcolor16m(string filter, string name)
{
    string path = "COLOR16M.MTX";

    readRawMatrix(path);

    BITMAPFILEHEADER bmfh = createFileHeader(0);
    BITMAPV5HEADER bmV5h = createPV5Header(24);

    if (filter == "f3x3"){
        filter3x3();
    }

    if (filter == "f5x5"){
        filter5x5();
    }

    if (filter == "difference"){
        filter_difference();
    }

    ofstream file (nameOfColor16mFile+name+".bmp",ios::binary|ios::out|ios_base::trunc);
    if (!file.bad()){
        file.write((const char *)&bmfh,sizeof(bmfh));
        file.write((const char *)&bmV5h,sizeof(bmV5h));
        file.write((const char *)BitmapFull,lengthOfReadFile);
        file.close();
    }
}

void MainWindow::openAndPrintBMPcolor16m()
{
    QPixmap pm(QString().fromStdString(nameOfColor16mFile)); // <- path to image file
    ui->labelImage->setPixmap(pm);
    ui->labelImage->setScaledContents(true);
}

void MainWindow::openAndPrintBMPcolor16m_3x3()
{
    QPixmap pm(QString().fromStdString(nameOfColor16mFile+"-3x3")); // <- path to image file
    ui->label_3x3->setPixmap(pm);
    ui->label_3x3->setScaledContents(true);
}

void MainWindow::openAndPrintBMPcolor16m_5x5()
{
    QPixmap pm(QString().fromStdString(nameOfColor16mFile+"-5x5")); // <- path to image file
    ui->label_5x5->setPixmap(pm);
    ui->label_5x5->setScaledContents(true);
}

void MainWindow::openAndPrintBMPcolor16m_difference()
{
    QPixmap pm(QString().fromStdString(nameOfColor16mFile+"-difference")); // <- path to image file
    ui->label_difference->setPixmap(pm);
    ui->label_difference->setScaledContents(true);
}

void MainWindow::filter3x3()
{
    memcpy(Bitmap,BitmapFull,lengthOfReadFile);

    for (int i = 1; i <= 478; i++) {
        for (int j = 1; j <= 638 ; j++) {
            BYTE colorB = (Bitmap[i-1][j+1][0] + Bitmap[i][j+1][0] + Bitmap[i+1][j+1][0] +
                           Bitmap[i][j+1][0]   + Bitmap[i][j][0]   + Bitmap[i][j-1][0]   +
                           Bitmap[i-1][j-1][0] + Bitmap[i][j-1][0] + Bitmap[i+1][j-1][0])/
                           9;
            BYTE colorG = (Bitmap[i-1][j+1][1] + Bitmap[i][j+1][1] + Bitmap[i+1][j+1][1] +
                           Bitmap[i][j+1][1]   + Bitmap[i][j][1]   + Bitmap[i][j-1][1]   +
                           Bitmap[i-1][j-1][1] + Bitmap[i][j-1][1] + Bitmap[i+1][j-1][1])/
                           9;
            BYTE colorR = (Bitmap[i-1][j+1][2] + Bitmap[i][j+1][2] + Bitmap[i+1][j+1][2] +
                           Bitmap[i][j+1][2]   + Bitmap[i][j][2]   + Bitmap[i][j-1][2]   +
                           Bitmap[i-1][j-1][2] + Bitmap[i][j-1][2] + Bitmap[i+1][j-1][2])/
                           9;
            Bitmap[i][j][0] = colorB;
            Bitmap[i][j][1] = colorG;
            Bitmap[i][j][2] = colorR;
        }
    }

    delete BitmapFull;
    BitmapFull = new BYTE[lengthOfReadFile];
    memcpy(BitmapFull,Bitmap,lengthOfReadFile);
}

void MainWindow::filter5x5()
{
    memcpy(Bitmap,BitmapFull,lengthOfReadFile);

    for (int i = 2; i <= 477; i++) {
        for (int j = 2; j <= 637 ; j++) {
            BYTE colorB = (Bitmap[i-2][j+2][0] + Bitmap[i-1][j+2][0] + Bitmap[i][j+2][0] + Bitmap[i+1][j+2][0] + Bitmap[i+2][j+2][0] +
                           Bitmap[i-2][j+1][0] + Bitmap[i-1][j+1][0] + Bitmap[i][j+1][0] + Bitmap[i+1][j+1][0] + Bitmap[i+2][j+1][0] +
                           Bitmap[i][j+2][0]   + Bitmap[i][j-1][0]   + Bitmap[i][j][0]   + Bitmap[i][j-1][0]   + Bitmap[i][j-2][0]   +
                           Bitmap[i-2][j-1][0] + Bitmap[i-1][j-1][0] + Bitmap[i][j-1][0] + Bitmap[i+1][j-1][0] + Bitmap[i+2][j-1][0] +
                           Bitmap[i-2][j-2][0] + Bitmap[i-1][j-2][0] + Bitmap[i][j-2][0] + Bitmap[i+1][j-2][0] + Bitmap[i+2][j-2][0])/
                           25;
            BYTE colorG = (Bitmap[i-2][j+2][1] + Bitmap[i-1][j+2][1] + Bitmap[i][j+2][1] + Bitmap[i+1][j+2][1] + Bitmap[i+2][j+2][1] +
                           Bitmap[i-2][j+1][1] + Bitmap[i-1][j+1][1] + Bitmap[i][j+1][1] + Bitmap[i+1][j+1][1] + Bitmap[i+2][j+1][1] +
                           Bitmap[i][j+2][1]   + Bitmap[i][j-1][1]   + Bitmap[i][j][1]   + Bitmap[i][j-1][1]   + Bitmap[i][j-2][1]   +
                           Bitmap[i-2][j-1][1] + Bitmap[i-1][j-1][1] + Bitmap[i][j-1][1] + Bitmap[i+1][j-1][1] + Bitmap[i+2][j-1][1] +
                           Bitmap[i-2][j-2][1] + Bitmap[i-1][j-2][1] + Bitmap[i][j-2][1] + Bitmap[i+1][j-2][1] + Bitmap[i+2][j-2][1])/
                           25;
            BYTE colorR = (Bitmap[i-2][j+2][2] + Bitmap[i-1][j+2][2] + Bitmap[i][j+2][2] + Bitmap[i+1][j+2][2] + Bitmap[i+2][j+2][2] +
                           Bitmap[i-2][j+1][2] + Bitmap[i-1][j+1][2] + Bitmap[i][j+1][2] + Bitmap[i+1][j+1][2] + Bitmap[i+2][j+1][2] +
                           Bitmap[i][j+2][2]   + Bitmap[i][j-1][2]   + Bitmap[i][j][2]   + Bitmap[i][j-1][2]   + Bitmap[i][j-2][2]   +
                           Bitmap[i-2][j-1][2] + Bitmap[i-1][j-1][2] + Bitmap[i][j-1][2] + Bitmap[i+1][j-1][2] + Bitmap[i+2][j-1][2] +
                           Bitmap[i-2][j-2][2] + Bitmap[i-1][j-2][2] + Bitmap[i][j-2][2] + Bitmap[i+1][j-2][2] + Bitmap[i+2][j-2][2])/
                           25;
            Bitmap[i][j][0] = colorB;
            Bitmap[i][j][1] = colorG;
            Bitmap[i][j][2] = colorR;
        }
    }

    delete BitmapFull;
    BitmapFull = new BYTE[lengthOfReadFile];
    memcpy(BitmapFull,Bitmap,lengthOfReadFile);
}

void MainWindow::filter_difference()
{
    BYTE Bitmap_3x3[480][640][3];
    BYTE Bitmap_5x5[480][640][3];

    memcpy(Bitmap,BitmapFull,lengthOfReadFile);
    for (int i = 1; i <= 478; i++) {
        for (int j = 1; j <= 638 ; j++) {
            BYTE colorB = (Bitmap[i-1][j+1][0] + Bitmap[i][j+1][0] + Bitmap[i+1][j+1][0] +
                           Bitmap[i][j+1][0]   + Bitmap[i][j][0]   + Bitmap[i][j-1][0]   +
                           Bitmap[i-1][j-1][0] + Bitmap[i][j-1][0] + Bitmap[i+1][j-1][0])/
                           9;
            BYTE colorG = (Bitmap[i-1][j+1][1] + Bitmap[i][j+1][1] + Bitmap[i+1][j+1][1] +
                           Bitmap[i][j+1][1]   + Bitmap[i][j][1]   + Bitmap[i][j-1][1]   +
                           Bitmap[i-1][j-1][1] + Bitmap[i][j-1][1] + Bitmap[i+1][j-1][1])/
                           9;
            BYTE colorR = (Bitmap[i-1][j+1][2] + Bitmap[i][j+1][2] + Bitmap[i+1][j+1][2] +
                           Bitmap[i][j+1][2]   + Bitmap[i][j][2]   + Bitmap[i][j-1][2]   +
                           Bitmap[i-1][j-1][2] + Bitmap[i][j-1][2] + Bitmap[i+1][j-1][2])/
                           9;
            Bitmap[i][j][0] = colorB;
            Bitmap[i][j][1] = colorG;
            Bitmap[i][j][2] = colorR;
        }
    }

    memcpy(Bitmap_3x3,Bitmap,lengthOfReadFile);

    memcpy(Bitmap,BitmapFull,lengthOfReadFile);
    for (int i = 2; i <= 477; i++) {
        for (int j = 2; j <= 637 ; j++) {
            BYTE colorB = (Bitmap[i-2][j+2][0] + Bitmap[i-1][j+2][0] + Bitmap[i][j+2][0] + Bitmap[i+1][j+2][0] + Bitmap[i+2][j+2][0] +
                           Bitmap[i-2][j+1][0] + Bitmap[i-1][j+1][0] + Bitmap[i][j+1][0] + Bitmap[i+1][j+1][0] + Bitmap[i+2][j+1][0] +
                           Bitmap[i][j+2][0]   + Bitmap[i][j-1][0]   + Bitmap[i][j][0]   + Bitmap[i][j-1][0]   + Bitmap[i][j-2][0]   +
                           Bitmap[i-2][j-1][0] + Bitmap[i-1][j-1][0] + Bitmap[i][j-1][0] + Bitmap[i+1][j-1][0] + Bitmap[i+2][j-1][0] +
                           Bitmap[i-2][j-2][0] + Bitmap[i-1][j-2][0] + Bitmap[i][j-2][0] + Bitmap[i+1][j-2][0] + Bitmap[i+2][j-2][0])/
                           25;
            BYTE colorG = (Bitmap[i-2][j+2][1] + Bitmap[i-1][j+2][1] + Bitmap[i][j+2][1] + Bitmap[i+1][j+2][1] + Bitmap[i+2][j+2][1] +
                           Bitmap[i-2][j+1][1] + Bitmap[i-1][j+1][1] + Bitmap[i][j+1][1] + Bitmap[i+1][j+1][1] + Bitmap[i+2][j+1][1] +
                           Bitmap[i][j+2][1]   + Bitmap[i][j-1][1]   + Bitmap[i][j][1]   + Bitmap[i][j-1][1]   + Bitmap[i][j-2][1]   +
                           Bitmap[i-2][j-1][1] + Bitmap[i-1][j-1][1] + Bitmap[i][j-1][1] + Bitmap[i+1][j-1][1] + Bitmap[i+2][j-1][1] +
                           Bitmap[i-2][j-2][1] + Bitmap[i-1][j-2][1] + Bitmap[i][j-2][1] + Bitmap[i+1][j-2][1] + Bitmap[i+2][j-2][1])/
                           25;
            BYTE colorR = (Bitmap[i-2][j+2][2] + Bitmap[i-1][j+2][2] + Bitmap[i][j+2][2] + Bitmap[i+1][j+2][2] + Bitmap[i+2][j+2][2] +
                           Bitmap[i-2][j+1][2] + Bitmap[i-1][j+1][2] + Bitmap[i][j+1][2] + Bitmap[i+1][j+1][2] + Bitmap[i+2][j+1][2] +
                           Bitmap[i][j+2][2]   + Bitmap[i][j-1][2]   + Bitmap[i][j][2]   + Bitmap[i][j-1][2]   + Bitmap[i][j-2][2]   +
                           Bitmap[i-2][j-1][2] + Bitmap[i-1][j-1][2] + Bitmap[i][j-1][2] + Bitmap[i+1][j-1][2] + Bitmap[i+2][j-1][2] +
                           Bitmap[i-2][j-2][2] + Bitmap[i-1][j-2][2] + Bitmap[i][j-2][2] + Bitmap[i+1][j-2][2] + Bitmap[i+2][j-2][2])/
                           25;
            Bitmap[i][j][0] = colorB;
            Bitmap[i][j][1] = colorG;
            Bitmap[i][j][2] = colorR;
        }
    }

    memcpy(Bitmap_5x5,BitmapFull,lengthOfReadFile);

    for (int i = 0; i <= 479; i++) {
        for (int j = 0; j <= 639 ; j++) {
            Bitmap[i][j][0] = abs(Bitmap_5x5[i][j][0] - Bitmap_3x3[i][j][0]);
            Bitmap[i][j][1] = abs(Bitmap_5x5[i][j][1] - Bitmap_3x3[i][j][1]);
            Bitmap[i][j][2] = abs(Bitmap_5x5[i][j][2] - Bitmap_3x3[i][j][2]);
        }
    }

    delete BitmapFull;
    BitmapFull = new BYTE[lengthOfReadFile];
    memcpy(BitmapFull,Bitmap,lengthOfReadFile);
}
