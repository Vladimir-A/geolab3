#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "./bmpheaders.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

using namespace std;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    string nameOfColor16mFile = "color16m";
    string nameOfColor16mMatrix = "COLOR16M.MTX";
    BYTE Bitmap[480][640][3];
    BYTE *BitmapFull;
    size_t lengthOfReadFile;
    RGBQUAD rgb[256];

    void readRawMatrix(string);
    BITMAPFILEHEADER createFileHeader(unsigned int);
    BITMAPV5HEADER createPV5Header(unsigned int);

    void starter();
    void createNewBMPcolor16m(string,string="");
    void openAndPrintBMPcolor16m();
    void openAndPrintBMPcolor16m_3x3();
    void openAndPrintBMPcolor16m_5x5();
    void openAndPrintBMPcolor16m_difference();

    void filter3x3();
    void filter5x5();
    void filter_difference();
};
#endif // MAINWINDOW_H
