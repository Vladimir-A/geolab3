#ifndef BMPHEADERS_H
#define BMPHEADERS_H

#include <string.h>
#include <fstream>

using namespace std;

typedef   uint_least8_t    BYTE;
typedef   uint_least16_t   WORD;
typedef   uint_least32_t   DWORD;

#pragma pack(push,1)
typedef struct {
    WORD	bfType;
    DWORD	bfSize;
    WORD	bfReserved1;
    WORD	bfReserved2;
    DWORD	bfOffbits;
} BITMAPFILEHEADER;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct {
    float ciexyzX;
    float ciexyzY;
    float ciexyzZ;
} CIEXYZ;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct {
    CIEXYZ ciexyzRed;
    CIEXYZ ciexyzGreen;
    CIEXYZ ciexyzBlue;
} CIEXYZTPLE;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct {
    DWORD	bV5Size;
    DWORD   bV5Width;
    DWORD   bV5Height;
    WORD    bV5Planes;
    WORD    bV5BitCount;
    DWORD   bV5Compression;
    DWORD   bV5SizeImage;
    DWORD   bV5XPelsPerMeter;
    DWORD   bV5YPelsPerMeter;
    DWORD   bV5ClrUsed;
    DWORD   bV5ClrImportant;
    DWORD   bV5RedMask;
    DWORD   bV5GreenMask;
    DWORD   bV5BlueMask;
    DWORD   bV5AlphaNask;
    DWORD   bV5CSType;
    CIEXYZTPLE bV5Endpoints;
    DWORD   bV5GammaRed;
    DWORD   bV5GammaGreen;
    DWORD   bV5GammaBlue;
    DWORD   bV5Intent;
    DWORD   bV5ProfileData;
    DWORD   bV5ProfileSize;
    DWORD   bV5Reserved;
} BITMAPV5HEADER;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct {
    BYTE	 rgbBlue;	// Интенсивность синего
    BYTE	 rgbGreen;	// Интенсивность зеленого
    BYTE	 rgbRed;	// Интенсивность красного
    BYTE	 rgbReserved;	// Не используется
} RGBQUAD;
#pragma pack(pop)

#endif // BMPHEADERS_H
